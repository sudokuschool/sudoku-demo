
import React, { Component } from 'react';
import ReactTable from 'react-table'
import classnames from 'classnames';

import {Grid} from './grid.js';
import {PencilMarks} from './grid.js';
import {Parent} from './Learn.js';

import './Layout.css';
import './Board.css';
import logo from '../src/logo.png';

export class Header extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="headerContainer">
          <div className="headerItem"> 
            <a href="https://sudokuschool.com"><img className="headerLogo" src={logo} alt="logo"></img></a>
          </div>
          <div className="headerItem"> 
            Sudoku School 
          </div>
        </div>
        </React.Fragment>
    );
  }
}


export class Footer extends Component {
  render() {
    return (
		<React.Fragment>
        <div className="footerContainer">
          <div className="footerItem"> 
            <a href="https://sudokuschool.com">Home</a> 
          </div>
          <div className="footerItem">  
            <a href="https://itunes.apple.com/us/app/sudoku-401/id292703166?mt=8">iOS version</a> 
          </div>
          <div className="footerItem">  
            <i> Have fun learning and playing Sudoku! </i>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export class Board extends Component {
  constructor () {
    super()
    this.state = {
      isHidden: true
    }
  }

  toggleHidden () {
    this.setState({
      isHidden: !this.state.isHidden
    })
  }

  render() {
    return (
		<React.Fragment>
        <div className="bodyContainer">
          <div className="learnItem"></div>
          <div className="boardItem">  
            <Grid />
            <div className="function"> 
               <button className="buttonFunction" onClick={this.toggleHidden.bind(this)}> Learn Mode </button>    
               <PencilMarks />   
               <NewGame />     
            </div> 
          </div>
          <div className="learnItem"> 
            {!this.state.isHidden && <Learn />}  
          </div>
        </div>
      </React.Fragment>
    );
  }
}

class NewGame extends React.Component {
    render() {
      return ( < button className="buttonFunction" onClick = {this._refreshPage} > New Game </button>);
      }
      _refreshPage() {
        console.log("Clicked");
        window.location.reload();
      }
}

const Learn = () => (
  <div>
    <h4>Full House</h4>
    <p>Full House is the most basic step to solve. Sudoku puzzle. When a house (Row, Column, or Block) contains 8 determined values, the blank cell can only be one value.</p>
  </div>
)

export class Game extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Board />
        <Footer />
      </React.Fragment>
    );
  }
}



