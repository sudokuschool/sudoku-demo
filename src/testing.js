export class Cell extends React.Component {
	constructor(props){
		super(props);
		let val_prop = ''  //change this to 1-9 for alternative starter cell
		let pencilMarks_prop = [1,2,3,4,5,6]
		this.handleChange = this.handleChange.bind(this)
		this.handleBlur = this.handleBlur.bind(this)
		this.handleClick = this.handleClick.bind(this)
		this.state = {
			curNumber: val_prop,
			cellType: 'defaultValue',
			originialNum: val_prop,
			autofocus: false,
			pencilMarks: pencilMarks_prop
			};
	}

	handleChange(event) {
		var curVal = event.target.value
  		this.setState({curNumber: curVal});
  	}

  	handleBlur(event) {
  		var curVal = event.target.value
  		var nextInfo =  this.getNextCellTypeAndVal(curVal)
  		if (nextInfo.type == 'emptySoPencilMarks'){
  			this.setState({cellType: nextInfo.type })
  		}
  		else {
	  		this.setState({curNumber: nextInfo.value,
	  						cellType: nextInfo.type,
	  						autofocus: false });
	  	}
  	}

  	getNextCellTypeAndVal(curVal){
  		if ((curVal > 0) && (curVal < 10)){
  			return {type: 'userEnteredVal',
  					value: curVal};
  		}
  		else if (this.state.originialNum != ''){
  			return {type: 'defaultValue',
  					value: this.state.originialNum};
  		}
  		else{
  			return {type: 'emptySoPencilMarks'};
  		}
  	}

  	handleClick(){
		this.setState({cellType: 'userEnteredVal',
  						curNumber: '',
  						autofocus: true});
	}

	render(){
		const curNumber = this.state.curNumber
		const cellType = this.state.cellType
		if (cellType != 'emptySoPencilMarks'){
			if (this.state.autofocus == true){
				return <input type="text" value={curNumber} onChange={this.handleChange} onBlur={this.handleBlur} className={cellType} autoFocus />;
			}
			else{
				return <input type="text" value={curNumber} onChange={this.handleChange} onBlur={this.handleBlur} className={cellType} />;
			}
		}
		else{
			return (
				<div className={cellType} onClick={this.handleClick} >
					<div>
						<small>
							{this.state.pencilMarks[0]+' '+this.state.pencilMarks[1]+' '+this.state.pencilMarks[2]}
						</small>
					</div>
		    		<div>
		    			<small>
		    				{this.state.pencilMarks[3]+' '+this.state.pencilMarks[4]+' '+this.state.pencilMarks[5]}
		    			</small>
		    		</div>
				</div>
			)
		}
	}
}



