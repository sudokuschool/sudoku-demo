import React from 'react';
import './Board.css';
import './Layout.css';
import {Board} from './Board.js';

var values = '016002400320009000040103000005000069009050300630000800000306010000400072004900680'
var board = null
var grid = []
var pencilOn = false

export class Grid extends React.Component {

	render() {
/*
	var values = [];
	var start = 0;
	var url = window.location.href; 
  	console.log(url);
  	for (let iter1 = 0; iter1 < url.length; iter1++) {
  		if (url[iter1] == "=") {
  			start = iter1;
  		} 
  		if (start > 0) {
  			if (url[iter1] == "/") { start = 0; }
  			else { console.log(url[iter1]); } // var[iter1] = url[iter1-start]; }
  		}
  	}
  	( grid[x][y] ) 
 */		

/* BOARD INSTEAD OF GRID
 	if (grid[0] == null) {
		for (let i=0; i<9; i++){
			var tmp = []
			for (let j=0; j<9; j++){
				tmp.push(values[(i*9)+j])
			}
			grid.push(tmp.slice())
		}	
		console.log('GRID')
	} */

	if (board == null) {
		board = new Board(values);
		console.log('BOARD')
	}

	return (
		<table>
				{
					board.get_board_values().map(function(row, x){ 
					return( <tr class="border_bottom">
						{row.map(function(elem, y){
							let obj = new Object()
							obj = { value: board.get_cell_value_xy(x, y), 
								xvalue: x, 
								yvalue: y };
						console.log(obj.xvalue );
						return (<td class="data" key={x+', '+y}> <Cell val = { obj } /> </td>)})}
					</tr>)})
				} 
		</table>
		)
	}
}

var pencil = 2;
export class Cell extends React.Component {
 	constructor(props){
	    super(props);
	    this.handleChange = this.handleChange.bind(this)
	    this.handleBlur = this.handleBlur.bind(this)
	    this.handleClick = this.handleClick.bind(this)
	    this.state = {
	      x: this.props.val.xvalue,
	      y: this.props.val.yvalue,
	      error: 'noError',
	      pencilActive: (pencil % 2 == 0) ? 'penTrue' : 'penFalse',
	      pencilMarks: [null, null, null, null, null, null, null, null, null],
	      pencils: null,
	      curNumber: (this.props.val.value == 0) ? '' : this.props.val.value,
	      cellType: ((this.props.val.value == 0) ? 'userEnteredVal' : 'defaultValue'),
	      originialNum: (this.props.val.value == 0) ? '' : this.props.val.value,
	    };
  	}

	handleChange(event) {
		console.log("HANDLE CHANGE");
	    var curVal = event.target.value
	    var i
	   	var j
	    var z
	    if (this.state.cellType == 'defaultValue')
	    	return
	    if (this.state.cellType == 'userEnteredVal') {
			console.log(pencil % 2);
	    	if ( pencil  % 2 == 0 ) { 
	    		this.state.pencilActive = 'penTrue' 
	    		this.setState({pencils: null});
	    		this.setState({pencilMarks: [null, null, null, null, null, null, null, null, null]});

			    if (curVal == '' || ((curVal > 0) && (curVal < 10))){
					this.state.error = 'noError'
					for (i = 0; i < 9; i++) {
						if ( board.get_cell_value_xy(this.state.x, i) == curVal ) { this.state.error = 'isError' }
					}

					for (j = 0; j < 9; j++) {
						if ( board.get_cell_value_xy(j, this.state.y) == curVal ) { this.state.error = 'isError' }
					}

				    this.setState({curNumber: curVal});
					for (i = 0; i < this.state.x; i++) {}	
					for (j = 0; j < this.state.y; j++) {}

					if (curVal == '') { board.set_cell_value_xy(i, j, 0); } 
					else { board.set_cell_value_xy(i, j, curVal) }

					var solved = false;
					solved = board.is_solved();
					/*for (i = 0; i < 9; i++) {	
						for (j = 0; j < 9; j++) {
							if (grid[i][j] == 0) {
								solved = false;
							}
						}
					}*/
					if (solved == true) { window.alert("You Won Congratulations"); }
				}
	    	} 
	    	else if ( pencil  % 2 == 1 ) { 
	    		this.state.pencilActive = 'penFalse' 
	    		var entryArray = curVal.split("")

    			for (z = 0; z < entryArray.length; z++) {
    				if (entryArray[z] == '' || ((entryArray[z] > 0) && (entryArray[z] < 10))){
    					this.state.pencilMarks[entryArray[z]] = entryArray[z];
	    			}
				}
				var str = this.state.pencilMarks.join(' ')
				var str1 = str.split(" ")
				str = str1.join(' ')
				this.setState({pencils: str});
	    	}
		}
	}

	handleBlur(event) {
		console.log("HANDLE BLUR");
	    var curVal = event.target.value
	    var nextInfo =  this.getNextCellTypeAndVal(curVal)
	}

	getNextCellTypeAndVal(curVal){
		console.log("GET  NEXT CELL TYPE");
	    if ((curVal < 0) || (curVal > 10)){
	        return {type: this.state.cellType,
	            value: this.state.originialNum};

	    } else if (this.state.cellType == 'defaultValue'){
	          return {type: 'defaultValue',
	            value: this.state.originialNum};
	    }
	}

	handleClick(){
		console.log("HANDLE CLICK");
	    this.setState({cellType: 'userEnteredVal',
	              curNumber: ''});
	}

	render() {
	  	console.log('CELL')
	  	console.log(this.state.pencilMarks)
	  	if ( this.state.error == 'isError' ) {
	  		console.log( this.state.error );
		    return <input type="text" 
		      value={this.state.curNumber} 
		      onChange={this.handleChange} 
		      onBlur={this.handleBlur} 
		      className={this.state.error} />
		}
	  	if ( this.state.pencilActive == 'penTrue' ) {
		    return <input type="text" 
		      value={this.state.curNumber} 
		      onChange={this.handleChange} 
		      onBlur={this.handleBlur} 
		      className={this.state.cellType} />
		}
	  	if ( this.state.pencilActive == 'penFalse' ) {
		    return <input type="text" 
		      value={this.state.pencils} 
		      onChange={this.handleChange} 
		      onBlur={this.handleBlur} 
		      className={this.state.pencilActive} />
		}
	}

}

export class PencilMarks extends React.Component {
  constructor () {
    super()
    this.state = {
      isHidden: true
    }
  }

  toggleHidden () {
  	pencil = pencil + 1
  	console.log(pencil);
    this.setState({
      isHidden: !this.state.isHidden
    })
  }

  render() {
    return (
		<React.Fragment>
            <div className="function"> 
               <button className="buttonFunction" onClick={this.toggleHidden.bind(this)}> Pencil Marks </button>   
            </div> 
      </React.Fragment>
    );
  }
}



