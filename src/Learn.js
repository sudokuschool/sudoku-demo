import React, { Component } from 'react';

/*

var learnModes = [   
  { mode: 'fullHouse',
  text: 'Full House is the most basic step to solve. Sudoku puzzle. When a house (Row, Column, or Block) contains 8 determined values, the blank cell can only be one value. Let&#39;s look at the example: Row 2 has eight cells filled, and only R2C8 is blank. The only possible value for R2C8 is 6.' },

  { mode: 'hiddenSingle',
  text: 'In a House (Row, Column, or Block), if there is only one possible position for a candidate, we call it hidden single. Let&#39;s look at the example: In Block 1, Column 2 and 3, and Row 3 cannot be 9. Thus, the only possible position for 9 is R2C1. An easier way to spot hidden single is to tap on a clue. Sudobility iPhone software will automatically gray out all the cells which are not possible for the candidate. In this example, by tapping clue 4, Sudobility shows the only possible location for 4 in Block 2 is R1C6. ' },

  { mode: 'nakedSingle',
  text: 'When there is only one candidate for a particular cell, it is called naked single. Let&#39;s look at the example: R3C9 can only be 9, since 1, 2, 3 and 5 exists in Column 9, 5 and 8 exist in Row 3, and 3, 4, 6 and 7 exist in Block 3. To spot Naked Single, often you have to look at all three Houses (Row, Column, and Block).' },
  
  { mode: 'hiddenPair',
  text: 'Hidden Pair is an advanced technique to remove candidates from certain cells. Pencil mark is required. In a House (Row, Column, or Block), when two candidates exist only in two cells, you can remove other candidates from those cells. Let&#39;s look at the example: In Row 9, candidates 5 and 8 can only exist in R9C4 and R9C9. If R9C4 is 5, then R9C9 is 8. If R9C4 is 8, then R9C9 is 5. You can remove candidates 3, 4, 6, and 7 from R9C9. '},

  { mode: 'nakedPair',
  text: 'In a House (Row, Column, or Block), if two cells contain the same two candidates and nothing else, you can remove those two candidates from other cells. Let&#39;s look at the example: In Column 8, The only two potential candidates for R2C8 and R5C8 are 2 and 6. If R2C8 is 2, then R5C8 is 6. If R2C8 is 6, then R5C8 is 2. In either case, R9C8 cannot be 2 or 6. '},

  { mode: 'lockedCandidates',
  text: 'Locked Candidates is an advanced single-digit technique to remove candidates from cells. Pencil mark is required. If a candidate in House (Row, Column or Block) 1 can only exist in the interception with House 2, you can remove the candidate from the other cells in House 2. Locked Candidates only happens with a Block and either a Row or a Column. Let&#39;s look at the example: In Block 6, candidate 2 can only exist in the interception between Block 6 and Column 9. Either R4C9 is 2, or R5C9 is 2, or R6C9 is 2. In any case, R7C9 cannot be 2.' },

  { mode: 'hiddenTriple',
  text: 'Hidden Triple extends the concept of Hidden Pair to three candidates in three cells. In a House (Row, Column, or Block), when three candidates exist only in three cells, you can remove other candidates from those cells.' },

  { mode: 'hiddenQuad',
  text: 'Hidden Quad extends the concept of Hidden Pair to four candidates in four cells. In a House (Row, Column, or Block), when four candidates exist only in four cells, you can remove other candidates from those cells.' },

  { mode: 'xwing',
  text: 'X-Wing is an advanced single-digit technique to remove candidates from cells. Pencil mark is required. For a particular candidate, if the possible positions in two rows are the same two columns, you can remove the candidate in the other rows of those two columns. For a particular candidate, if the possible positions in two columns are the same two rows, you can remove the candidate in the other columns of those two row. Let&#39;s look at the example: In Row 1 and 8, the only possible positions for candidate 7 is column 1 and 5. If R1C1 is 7, then R8C5 is 7. If R1C5 is 7, then R8C1 is 7. In any case, in Column 5, candidate 7 cannot exist other than in Row 1 and 8. You can remove 7 from R2C5 and R9C5.' },

  { mode: 'nakedTriple',
  text: 'In a House (Row, Column, or Block), if three cells contain three candidates and nothing else, you can remove those three candidates from other cells. To understand Naked Triple, please understand Naked Pair first. Naked Triple extends the concept to three candidates in three cells. Let&#39;s look at the example: In Block 5, The only three potential candidates for R5C4, R6C4 and R6C6 are 2, 3 and 7. You can remove 2, 3, and 7 from R4C4, R4C5, R4C6 and R5C5.' },

  { mode: 'nakedQuad',
  text: 'In a House (Row, Column, or Block), if four cells contain four candidates and nothing else, you can remove those four candidates from other cells. To understand Naked Quad, please understand Naked Pair first. Naked Quad extends the concept to four candidates in three cells.' },

  { mode: 'swordfish',
  text: 'Swordfish is an advanced single-digit technique to remove candidates from cells. Pencil mark is required. To understand Swordfish, please first understand X-Wing. Swordfish extend X-Wing into three rows and three columns. For a particular candidate, if the possible positions in three rows are the same three columns, you can remove the candidate in the other rows of those three columns. For a particular candidate, if the possible positions in three columns are the same three rows, you can remove the candidate in the other rows of those three columns.' },

  { mode: 'jellyfish',
  text: 'Jellyfish is an advanced single-digit technique to remove candidates from cells. Pencil mark is required. To understand Jellyfish, please first understand X-Wing. Jellyfish extend X-Wing into four rows and four columns. For a particular candidate, if the possible positions in four rows are the same four columns, you can remove the candidate in the other rows of those four columns. For a particular candidate, if the possible positions in four columns are the same four rows, you can remove the candidate in the other rows of those four columns.' },

  { mode: 'xywing',
  text: 'You are looking for three cells, with XZ, XY, and YZ pattern. Cell 1 and 2 share a house. Cell 2 and 3 share a house. Once the pattern is established, look for Cell 4 which contains Z, and visible to both Cell 1 and Cell 3. If Cell 1 is Z, then Cell 4 cannot be Z. If Cell 1 is X, then Cell 2 is Y, and Cell 3 is Z. Cell 4 still cannot be Z. Let&#39;s look at the example: R1C9, R2C7 and R9C7 form an XY-Wing. We can say X is 8, Y is 3, and Z is 1. If R1C9 is 1, then R3C7 and R8C9 cannot be 1. If R1C9 is 8, then R2C7 is 3 and R9C7 is 1. R3C7 and R8C9 cannot be 1 either. You can remove 1 from those two cells. You can also consider XY- Wing as the simplest case for ASL. R1C9 is a bi-value single cell ASL. R2C7 and R9C7 is the second ALS. 8 is the restricted common, and 1 is the common candidate which you can eliminate from shared visible cells of R1C9 and R9C7.' },

  { mode: 'squirmbag',
  text: 'Squirmbag is an advanced single-digit technique to remove candidates from cells. Pencil mark is required. To understand Squirmbag, please first understand X-Wing. Squirmbag extend X-Wing into five rows and five columns or more. For a particular candidate, if the possible positions in N rows are the same N columns, you can remove the candidate in the other rows of those columns. For a particular candidate, if the possible positions in N columns are the same N rows, you can remove the candidate in the other rows of those columns.' },

  { mode: 'xwingFinned',
  text: 'Finned X-Wing is a X-Wing with a fin, and you can remove the candidates which are both visible to the fin and X-Wing. Let&#39;s look at the following example: Row 1 and 4 would form a X-Wing for candidate 2, if R3C9 is not 2. Thus, either Row 1 and 4 is a X-Wing, or R3C9 is 2. In either case, R6C8 cannot be 2.' },

  { mode: 'swordfishFinned',
  text: 'Finned Swordfish is a Swordfish with a fin, and you can remove the candidates which are both visible to the fin and Swordfish. To understand the fin, please see Finned X-Wing. Let&#39;s look at the following example: Column 1, 8, 9 would form a Swordfish for candidate 8, if R9C8 is not 8. Thus, either Column 1, 8, 9 is a Swordfish, or R9C8 is 8. In either case, R7C7 cannot be 8.' },

  { mode: 'jellyfishFinned',
  text: 'Finned Jellyfish is a Jellyfish with a fin, and you can remove the candidates which are both visible to the fin and Jellyfish. To understand the fin, please see Finned X-Wing.' },

  { mode: 'xyzwing',
  text: 'XYZ-Wing is a variation of XY-Wing. Rather than looking for a XZ-XY-YZ pattern, the second contains XYZ, so it would be XZ-XYZ-YZ pattern, where XZ and XYZ share a house, XYZ and YZ share a house. Then, you can eliminate Z from any cells which are visible to all three cells in the XYZ-Wing.' },

  { mode: 'wxyzwing',
  text: 'WXYZ-Wing is a simplified case of ALS, and just a little harder to spot than XY-Wing. You are looking for bi-value cell and three cells which is an ALS. Let&#39;s look at the example: The blue bi-value cell and the green ALS share the restrict common candidate 9. Between those two ALS&#39;s, candidate 4 exists at R7C7 and R5C9. R6C7 and R9C7 are visible to both cells. You can remove 4 from R6C7 and R9C9. To be exact, if R7C7 is 4, then R6C7 and R9C9 cannot be 4. If R7C7 is 9, that forces the green cells into a naked triple of 2, 3 and 4. in this c.se, R6C7 and R9C9 cannot be 4 either. You can remove 4 from those two cells.' },

  { mode: 'almostLS',
  text: 'An Almost Locked Set is a group of N cells in a single house with candidates for N+1 digits. In other words, it is one cell short from being a locked set. A Common Restricted Candidate between two ALS&#39;s is a candidate which cannot exist in both ALS&#39;s, thus, its existence in one ALS will force the other ALS to become a locked set. When solving Sudoku, you are looking for two ALS&#39;s with a common restricted candidate, then look for a candidate in another cell which is visible to all the cells in the two ALS&#39;s with that candidate. Let&#39;s look at the following example: The green cells form one ALS. The blue cells form the second ALS. 5 is the common restricted value. Candidate 1 exists at R1C2 and R4C6, in those two ALS&#39;s. R1C6 is visible to both cells. You can eliminate 1 from R1C2. The logic is that if the green ALS contains 1 at R1C2, the the highlighted cell at R1C6 cannot be 1. If the green ALS does not cont.in 1, then it is a locked set of 3, 4 and 5, forcing the blue ALS to a locked set of 1, 4 and 9. Then, the blue ALS contains 1 at R4C6. R1C6 cannot be 1 either.' },

  { mode: 'swordfish',
  text: 'Finned Squirmbag is a Squirmbag with a fin, and you can remove the candidates which are both visible to the fin and Squirmbag. To understand the fin, please see Finned X-Wing.' }
]

learnModes.propTypes = {
  mode: React.PropTypes.string,
  text: React.PropTypes.string
}

*/

export class Learn extends Component {
  render() {
    return (
      <React.Fragment>  
      <div>
        <h4>Full House</h4>
        <p>Full House is the most basic step to solve. Sudoku puzzle. When a house (Row, Column, or Block) contains 8 determined values, the blank cell can only be one value.</p>
      </div>
      </React.Fragment>       
    );
  }
}

// export class Button extends Component {
//   handleClick() {
//     console.log('Click happened');
//   }
//   render() {
//     return <button>Learn Mode</button>;
//   }
// }

/*export class Child extends React.Component {
  componentDidMount() {
    this.props.onRef(this)
  }
  componentWillUnmount() {
    this.props.onRef(undefined)
  }
  method() {
    
  }
  render() {
    return (
    <React.Fragment>  
      <div>
        <h4>Full House</h4>
        <p>Full House is the most basic step to solve. Sudoku puzzle. When a house (Row, Column, or Block) contains 8 determined values, the blank cell can only be one value.</p>
      </div>
      </React.Fragment> 
    )
  }
}*/



